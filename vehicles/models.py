from django.db import models


class Vehicle(models.Model):
    AUTOMATIC = "automatic"
    MANUAL = "manual"
    TRANSMISSION_CHOICES = [
        (AUTOMATIC, "Automatic"),
        (MANUAL, "Manual"),
    ]
    #brand varchar(100) not null
    brand = models.CharField(max_length=100, null=False)
    model = models.CharField(max_length=100, null=False)
    year = models.PositiveBigIntegerField(null=False)
    transmission = models.CharField(max_length=32,null=True, choices=TRANSMISSION_CHOICES)

    def __str___(self):
        return f'{self.brand} {self.model}'