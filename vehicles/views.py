from django.shortcuts import render
from vehicles.models import Vehicle

def get_vehicles(request):
    vehicles = Vehicle.objects.all()
    return render (request, "vehicle_list.html", context={
        "vehicles": vehicles
    })

def get_vehicle_details(request,pk):
    vehicle = Vehicle.objects.get(pk = pk)
    return render(request, "vehicle_detail.html", context={
        "vehicle": vehicle
    })

