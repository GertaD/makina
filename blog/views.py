from django.shortcuts import render
from blog.models import Post
from blog.forms import PostForm


def get_post_list(request):
    post = Post.objects.all()
    return render(request, "post_list.html", context={
        "posts": post
    })

def get_post_details(request,pk):
    posts = Post.objects.get(pk = pk)
    return render(request, "post_details.html", context={
        "post": posts
    })

def add_new_post(request):
    print("request.method= ", request.method)
    print("request.GET =", request.GET)
    print("request.POST=", request.POST)

    if request.method == "POST":
        form = PostForm(request.POST)
    else:
        form = PostForm()

    return render(request, "post_add.html", context={
        "form" : form

    })