from django import forms
from django.forms import widgets, widgets

class PostForm(forms.Form):
    title = forms.CharField(max_length=255,)
    content = forms.CharField(widget=widgets.Textarea)

