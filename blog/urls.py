from django.urls import path

from blog.views import get_post_list, get_post_details, add_new_post

urlpatterns = [
    path("blog", get_post_list, name="post_list"),
    path("blog/add/", add_new_post, name="post_add"),
    path("blog/<int:pk>", get_post_details , name = 'post_details'),

]


